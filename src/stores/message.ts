import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const message = ref("");
  const timeout = ref(2000);
  const Showmessage = (msg: string, Tout: number = 2000) => {
    message.value = msg;
    isShow.value = true;
    timeout.value = Tout;
  };

  const Closemessage = () => {
    message.value = "";
    isShow.value = true;
  };

  return { isShow, message, Showmessage, Closemessage, timeout };
});
